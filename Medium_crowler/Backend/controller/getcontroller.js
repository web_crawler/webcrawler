const { get } = require("./routes");
const fetch=require("node-fetch")
const cheerio=require("cheerio");
const fs=require("fs");
const session=require("express-sessions")
const { title } = require("process");
const { render } = require("../../app");
var obj=[];
const getindex=(req,res)=>{
    res.render("login");
}
const getingblogs=async (req,res)=>{
    const tagname=req.body.tag_name;
    if(tagname=="")
    {
        res.redirect("/getblogs?isnull=false");
    }
    else{
        var response=await fetch("https://medium.com/_/api/tags/"+tagname+"//stream?limit=0");
        var html=await(response.text());
        html=html.toString();
        var arr=html.split(",");
        var stack1=[];
        var stack2=[];
        var stack3=[];
        var stack4=[];
          for(let i=0; i<arr.length; i++)
          {
              if(arr[i].includes("title")){
                  stack1.push(arr[i]);
              }
              if(arr[i].includes("description")){
                stack2.push(arr[i]);
              }
              if(arr[i].includes("tags")){
                stack3.push(arr[i]);
              }
              if(arr[i].includes("text")){
                  stack4.push(arr[i]);
                  
              }       

          }
          
          for(let i=0; i<stack1.length; i++)
          {
              if(stack2[i]==undefined){
                obj.push({title:stack1[i],description:"",tags:stack3[i],text:stack4[i]});
              }
              else if(stack3[i]==undefined){
                obj.push({title:stack1[i],description:stack2[i],tags:"",text:stack4[i]});
              }
              else if(stack4[i]==undefined){
                obj.push({title:stack1[i],description:stack2[i],tags:"",text:""});
              }
              else if(stack2[i]==undefined && stack3[i]==undefined){
                obj.push({title:stack1[i],description:"",tags:"",text:""});
              }
              else if(stack3[i]==undefined && stack4[i]==undefined){
                obj.push({title:stack1[i],description:"",tags:"",text:""});
              }
              else{
                obj.push({title:stack1[i],description:stack2[i],tags:stack3[i],text:stack4[i]});
              }
             
          }
          
          
            // const $=cheerio.load(html);
            // console.log("url is:",html.links);
           // console.log("arr is:",arr);
            // const links= $("a").map((i,link)=>link.attribs.href).get();
            //     console.log("links is:",links);
                // const images= $("img").map((i,link)=>link.attribs.src).get();
                // console.log("images is:",images);
               console.log("obj[o]",obj[0].title)
         res.redirect("/getblogs?displayblogs="+true);
       
    }
    }
const getblogs=(req,res)=>{
    if(req.query.isnull==false){
        res.render("blogs",{bol:"false"});
    }
    else{
        if(req.query.displayblogs){
            console.log("Now obj is:",obj);
            //console.log("query is:",req.query.displayblogs[0]);
            res.render("blogs",{bol:"true",blogs:obj});
        }
        else{
            res.render("blogs",{bol:"true",blogs:""});
        }
        
    }
    
} 
const getregister=(req,res)=>{
  res.render("Register");
}
module.exports={
    getindex:getindex,
    getblogs:getblogs,
    getingblogs:getingblogs,
    getregister:getregister
}